# Stage 1: Build the application
FROM gradle:8.6.0-jdk17 AS build

# Copy source code to the container
COPY --chown=gradle:gradle . /home/gradle/src

# Change working directory
WORKDIR /home/gradle/src

# Build the application using Gradle
RUN gradle clean build -x test

# Stage 2: Create the runtime image
FROM eclipse-temurin:17-jdk-alpine

# Add Maintainer Info
LABEL maintainer="kkousen@trincoll.edu"

# Add a volume pointing to /tmp
VOLUME /tmp

# Make port 8080 available to the world outside this container
EXPOSE 8080

# Copy the built jar file from the build stage to the container
COPY --from=build /home/gradle/src/build/libs/shopping-1.0.jar app.jar

# Run the jar file
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
